﻿using System;
using UnityEngine;

//This script controls the scrolling of the background
public class Background : BaseBehaviour
{
    [SerializeField]
    float _speed = 0.1f;          //Speed of the scrolling

    private float _startTime;
    private float _accumulator;

    enum eMoveState
    {
        Idle,
        MoveDown,
        MoveUp,
    }

    private Action[,] _stateActions;

    enum eAction
    {
        Update,
    }

    private eMoveState _currentMoveState = eMoveState.Idle;

    void Awake()
    {
        _stateActions = new Action[,]
       {
                            /*Update*/
            /*Idle*/        { () => { } },
            /*MoveDown*/    { MoveDownUpdate },
            /*MoveUp*/      { MoveUpUpdate },
       };
        Reset();
    }

    void Update()
    {
        _stateActions[(int)_currentMoveState, (int)eAction.Update]();
    }

    void Reset()
    {
        Vector2 offset = new Vector2(0, 0);
        //Apply the offset to the material
        renderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }

    void OnDisable()
    {
        Reset();
    }

    void Move()
    {
        _accumulator += Time.deltaTime;
        //Keep looping between 0 and 1
        float y = Mathf.Repeat(_accumulator * _speed, 1);
        //Create the offset
        Vector2 offset = new Vector2(0, y);
        //Apply the offset to the material
        renderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }

    private void MoveUpUpdate()
    {
        Move();
    }

    private void MoveDownUpdate()
    {
        Move();
    }

    public void StartMovingDown()
    {
        _currentMoveState = eMoveState.MoveDown;
        _speed = 0.2f;
    }

    public void StartMovingUp()
    {
        _currentMoveState = eMoveState.MoveUp;
        _speed = -0.2f;
    }

    public void StopMoving()
    {
        _currentMoveState = eMoveState.Idle;
        _speed = 0;
    }
}