using UnityEngine;
using Zenject;

public class BaseSceneInstaller : MonoInstaller<BaseSceneInstaller>
{
    public override void InstallBindings()
    {
        Container.BindAllInterfacesAndSelf<GameHandler>().To<GameHandler>().AsSingle().NonLazy();

        Container.BindSignal<HitFishSignal>();
        Container.BindTrigger<HitFishSignal.Trigger>();

        Container.BindSignal<HitGroundSignal>();
        Container.BindTrigger<HitGroundSignal.Trigger>();

    }
}