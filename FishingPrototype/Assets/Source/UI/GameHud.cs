﻿
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameHud : BaseBehaviour
{
    [SerializeField]
    private Text _scoreText;

    private int _score;

    public void OnRestart()
    {
        SceneManager.LoadScene(0);
    }

    public void AddScore(int score)
    {
        _score += score;
        _scoreText.text = "Score :: " + _score;
    }
}
