﻿
using System;
using System.Collections;
using UnityEngine;
using Zenject;

public class PlayerHook : BaseBehaviour
{
    [SerializeField]
    private float _yHeightToStartFalling = 24f; //in world pos y

    [SerializeField]
    private float _yHeightToFall = 12f; //in world pos y

    [SerializeField]
    private float _moveSpeed = 0.25f; //in world pos y

    [SerializeField]
    private float _buffer = 0.1f; //in world pos y

    [SerializeField]
    private float _sideWaysMove = 30f; //in world pos y

    [SerializeField]
    private float _waitBeforeMovingUp = 3f;

    private HitFishSignal.Trigger _hitFishSignalTrigger;
    private HitGroundSignal.Trigger _hitGroundSignalTrigger;

    [SerializeField]
    private Transform _caughtFishHolder;

    enum eState
    {
        Idle,
        FastMoveDown,
        MoveDown,
        MoveUp,
    }

    private Action[,] _stateActions;

    enum eAction
    {
        Update,
        Collision,
    }

    private eState _currentState = eState.Idle;
    private Collider _collider;
    private GameHandler _gameHandler;

    private void Awake()
    {
        _stateActions = new Action[,]
        {
                            /*Update*/              /*Collision*/
            /*Idle*/        { IdleUpdate,           IdleCollision },
            /*FastMoveDown*/{ FastMoveDownUpdate,   () => { _collider = null; } },
            /*MoveDown*/    { MoveDownUpdate,       DownCollision},
            /*MoveUp*/      { MoveUpUpdate,         UpCollision},
        };
    }

    [Inject]
    void Construct(HitFishSignal.Trigger fishTrigger, HitGroundSignal.Trigger groundTrigger, GameHandler gameHandler)
    {
        _hitFishSignalTrigger = fishTrigger;
        _hitGroundSignalTrigger = groundTrigger;
        _gameHandler = gameHandler;
    }

    void Update()
    {
        _stateActions[(int)_currentState, (int)eAction.Update]();
    }

    private void FastMoveDownUpdate()
    {
        transform.localPosition += new Vector3(0, _moveSpeed * -1 * 2, 0);
        if(transform.position.y <= _yHeightToFall)
            _currentState = eState.MoveDown;
    }

    private void MoveUpUpdate()
    {
        MoveToMouse();
        transform.localPosition += new Vector3(0, _moveSpeed, 0);
    }

    private void UpCollision()
    {
        if (_collider.tag == eTags.Ground.ToString())
        {
            _hitGroundSignalTrigger.Fire();
            _currentState = eState.Idle;
            _gameHandler.GotHitByGround();
        }
        else if(_collider.tag == eTags.Fish.ToString())
        {
            _collider.gameObject.GetComponent<Fish>().AttachToHook(_caughtFishHolder);
        }
        _collider = null;
    }

    private void MoveDownUpdate()
    {
        MoveToMouse();
        transform.localPosition += new Vector3(0, _moveSpeed * -1, 0);
    }

    private void MoveToMouse()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float sign = (worldPoint.x - transform.localPosition.x) > 0 ? 1 : -1;
        float magnitude = Mathf.Abs(worldPoint.x - transform.localPosition.x) > _buffer ? _sideWaysMove : 0;
        rigidbody.velocity = new Vector3(sign * magnitude, 0, 0);
    }

    private void DownCollision()
    {
        if(_collider.tag == eTags.Fish.ToString())
        {
            _hitFishSignalTrigger.Fire();
            _collider.gameObject.GetComponent<Fish>().AttachToHook(_caughtFishHolder);
            _currentState = eState.Idle;
            _gameHandler.GotFirstFish();
        }
        _collider = null;
    }

    private void IdleUpdate()
    {
        //MoveToMouse();
        if(transform.position.y >= _yHeightToStartFalling)
            _currentState = eState.FastMoveDown;
    }

    private void IdleCollision()
    {
        if (_collider.tag == eTags.Fish.ToString())
        {
            _collider.gameObject.GetComponent<Fish>().AttachToHook(_caughtFishHolder);
        }
        _collider = null;
    }

    void OnTriggerEnter(Collider other)
    {
        _collider = other;
        _stateActions[(int)_currentState, (int)eAction.Collision]();
    }

    public void StartMovingUp()
    {
        StartCoroutine(MoveUpInSomeTime());
    }

    private IEnumerator MoveUpInSomeTime()
    {
        yield return new WaitForSeconds(_waitBeforeMovingUp);
        _currentState = eState.MoveUp;
    }
}
