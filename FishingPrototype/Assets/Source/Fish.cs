﻿using UnityEngine;

public class Fish : BaseBehaviour
{
    [SerializeField]
    private float _scaleMin = 1;

    [SerializeField]
    private float _scaleMax = 3;

    [SerializeField]
    private float _xMag = 8;

    [SerializeField]
    private float _xMin = 1;

    [SerializeField]
    private float _xMax = 10;

    [SerializeField]
    private float _yMax = 20;

    [SerializeField]
    private int _score = 1;

    private HitGroundSignal _hitGroundSignal;

    private bool _attachedToHook = false;
    private bool _moveAround;
    private float _randomSeed;
    private float _speedToMove;

    public void SetRandomValues()
    {
        Vector3 scale = transform.localScale;
        float value = UnityEngine.Random.Range(_scaleMin, _scaleMax);
        transform.localScale = new Vector3(value, value, scale.z);
        if(UnityEngine.Random.value > 0.5f)
        {
            _moveAround = true;
            _speedToMove = UnityEngine.Random.Range(_xMin, _xMax);
            _randomSeed = UnityEngine.Random.Range(_xMin, _xMax);
        }
        else
        {
            _moveAround = false;
        }
    }

    public void AttachToHook(Transform fishHook)
    {
        _moveAround = false;
        transform.SetParent(fishHook, true);
        transform.localPosition = Vector3.zero;
        _attachedToHook = true;
    }

    public void SetSignals(HitGroundSignal hitGroundSignal)
    {
        _hitGroundSignal = hitGroundSignal;
        _hitGroundSignal.Event += HitByGround;
    }

    void Update()
    {
        if(_moveAround)
        {
            Vector3 pos = transform.localPosition;
            pos.x = Mathf.PingPong(Time.time * _speedToMove + _randomSeed, _xMag * 2) - _xMag;
            transform.localPosition = pos;
        }
    }

    void OnDisable()
    {
        _hitGroundSignal.Event -= HitByGround;
    }

    private void HitByGround()
    {
        if(_attachedToHook)
        {
            ThrowUpInAir();
        }
    }

    private void ThrowUpInAir()
    {
        transform.SetParent(null, true);
        Vector3 currentPos = transform.localPosition;
        currentPos.x = UnityEngine.Random.Range(_xMin, _xMax);
        currentPos.y = UnityEngine.Random.Range(currentPos.y + 5, _yMax);
        transform.localPosition = currentPos;
    }

    public int IsHitGood()
    {
        if(_attachedToHook)
        {
            Destroy(gameObject);
            return _score;
        }
        return 0;
    }
}
