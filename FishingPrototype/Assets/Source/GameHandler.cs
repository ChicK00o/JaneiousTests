﻿
using System;
using UnityEngine;
using Zenject;

public enum eTags
{
    PlayerHook,
    Ground,
    Fish,
}

public class GameHandler : IInitializable, ITickable
{

    private AllObjectMover _allObjectMover;
    private Background _background;
    private FishSpawner _fishSpawner;
    private PlayerHook _playerHook;
    private GameHud _gameHud;

    private float _timeToWait = 1f;
    private float _accumalatedTime;
    private Action _toCallAfterWait;

    enum eMoveState
    {
        Idle,
        MoveDown,
        MoveUp,
    }

    private Action[,] _stateActions;

    enum eAction
    {
        Update,
    }

    private eMoveState _currentMoveState = eMoveState.Idle;

    public void Initialize()
    {
        _stateActions = new Action[,]
        {
                            /*Update*/
            /*Idle*/        { IdleUpdate },
            /*MoveDown*/    { () => {} },
            /*MoveUp*/      { () => {} },
        };
    }

    [Inject]
    public GameHandler(AllObjectMover allObjectMover, Background background, FishSpawner fishSpawner,
        PlayerHook playerHook, GameHud gameHud)
    {
        _allObjectMover = allObjectMover;
        _background = background;
        _fishSpawner = fishSpawner;
        _playerHook = playerHook;
        _gameHud = gameHud;
        _currentMoveState = eMoveState.Idle;
        _accumalatedTime = 0;
        _toCallAfterWait = StartDown;
    }

    public void Tick()
    {
        _stateActions[(int)_currentMoveState, (int)eAction.Update]();
    }

    private void IdleUpdate()
    {
        if(_toCallAfterWait != null)
        {
            _accumalatedTime += Time.deltaTime;
            if(_accumalatedTime > _timeToWait)
            {
                _toCallAfterWait();
            }
        }
        else
        {
            CheckMouseClicks();
        }
    }
    
    public void GotFirstFish()
    {
        _allObjectMover.StopMoving();
        _background.StopMoving();
        _fishSpawner.StopSpawning();
        _accumalatedTime = 0;
        _toCallAfterWait = StartUp;
        _currentMoveState = eMoveState.Idle;
    }

    private void StartUp()
    {
        _allObjectMover.StartMovingUp();
        _background.StartMovingUp();
        _fishSpawner.StopSpawning();
        _playerHook.StartMovingUp();
        _currentMoveState = eMoveState.MoveUp;
    }

    private void StartDown()
    {
        _allObjectMover.StartMovingDown();
        _background.StartMovingDown();
        _fishSpawner.StartSpawning();
        _currentMoveState = eMoveState.MoveDown;
    }

    public void GotHitByGround()
    {
        _allObjectMover.StopMoving();
        _background.StopMoving();
        _fishSpawner.StopSpawning();
        _toCallAfterWait = null;
        _currentMoveState = eMoveState.Idle;
    }


    private void CheckMouseClicks()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if(hit.collider.tag == eTags.Fish.ToString())
                {
                    _gameHud.AddScore(hit.collider.gameObject.GetComponent<Fish>().IsHitGood());
                }
            }
        }
    }
}
