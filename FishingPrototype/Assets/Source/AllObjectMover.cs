﻿

using System;
using UnityEngine;

public class AllObjectMover : BaseBehaviour
{
    [SerializeField]
    private float _moveSpeed = 0.2f;

    enum eMoveState
    {
        Idle,
        MoveDown,
        MoveUp,
    }

    private Action[,] _stateActions;

    enum eAction
    {
        Update,
    }

    private eMoveState _currentMoveState = eMoveState.Idle;
        
    private void Awake()
    {
        _stateActions = new Action[,]
        {
                            /*Update*/
            /*Idle*/        { () => { } },
            /*MoveDown*/    { MoveDownUpdate },
            /*MoveUp*/      { MoveUpUpdate },
        };
    }

    void Update()
    {
        _stateActions[(int) _currentMoveState, (int) eAction.Update]();
    }

    private void MoveUpUpdate()
    {
        transform.localPosition -= new Vector3(0, _moveSpeed, 0);
    }

    private void MoveDownUpdate()
    {
        transform.localPosition += new Vector3(0, _moveSpeed, 0);
    }

    public void StartMovingDown()
    {
        _currentMoveState = eMoveState.MoveDown;
    }

    public void StartMovingUp()
    {
        _currentMoveState = eMoveState.MoveUp;
    }

    public void StopMoving()
    {
        _currentMoveState = eMoveState.Idle;
    }
}
