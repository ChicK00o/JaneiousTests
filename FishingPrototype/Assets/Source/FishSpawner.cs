﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FishSpawner : BaseBehaviour
{
    [SerializeField]
    private List<GameObject> _fishPrefabs;

    [SerializeField]
    private Transform _spawnFishHolder;

    [SerializeField]
    private float _xMagnitude = 8f;

    private float _waitTime;
    private float _accumulatedTime;

    private HitGroundSignal _hitGroundSignal;
    
    enum eState
    {
        Idle,
        Spawn,
    }

    private Action[,] _stateActions;

    enum eAction
    {
        Update,
    }

    private eState _currentState = eState.Idle;

    private void Awake()
    {
        _stateActions = new Action[,]
        {
                            /*Update*/
            /*Idle*/        { () => {} },
            /*Spawn*/       { SpawnUpdate },
        };
    }

    [Inject]
    void Construct(HitGroundSignal groundSignal)
    {
        _hitGroundSignal = groundSignal;
    }

    private void SpawnUpdate()
    {
        _accumulatedTime += Time.deltaTime;
        if(_accumulatedTime > _waitTime)
        {
            SetWaitTime();
            SpawnRandomFish();
        }
    }

    private void SpawnRandomFish()
    {
        GameObject fishGo = Instantiate(_fishPrefabs[UnityEngine.Random.Range(0, _fishPrefabs.Count)]);
        fishGo.transform.position = new Vector3(UnityEngine.Random.Range(-8.0f, 8.0f), transform.position.y, transform.position.z);
        fishGo.transform.SetParent(_spawnFishHolder, true);
        Fish fish = fishGo.GetComponent<Fish>();
        fish.SetRandomValues();
        fish.SetSignals(_hitGroundSignal);
    }

    void Update()
    {
        _stateActions[(int)_currentState, (int)eAction.Update]();
    }

    public void StopSpawning()
    {
        _currentState = eState.Idle;
    }

    public void StartSpawning()
    {
        _currentState = eState.Spawn;
        SetWaitTime();
    }

    private void SetWaitTime()
    {
        _waitTime = UnityEngine.Random.Range(0.001f, 1);
        _accumulatedTime = 0;
    }
}
