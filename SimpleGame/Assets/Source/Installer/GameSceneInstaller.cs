using UnityEngine;
using Zenject;

public class GameSceneInstaller : MonoInstaller<GameSceneInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<GameHandler>().To<GameHandler>().AsSingle();
    }
}