﻿using UnityEngine;
using Zenject;

public class Ground : BaseBehaviour
{
    private GameHandler _gameHandler;

    [Inject]
    void Construct(GameHandler gameHandler)
    {
        _gameHandler = gameHandler;
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Ball")
        {
            _gameHandler.LostGame();
        }
    }
}
