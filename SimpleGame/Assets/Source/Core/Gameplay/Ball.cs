﻿using System.Collections;
using UnityEngine;

public class Ball : BaseBehaviour
{
    [SerializeField]
    private float _forceMagnitude;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);

        Vector2 direction;
        do
        {
            direction = Random.insideUnitCircle;
        } while (direction.y < 0.5f);

        rigidbody2D.AddForce(direction * _forceMagnitude, ForceMode2D.Impulse);
    }

    public void StopMovement()
    {
        rigidbody2D.velocity = Vector2.zero;
        rigidbody2D.angularVelocity = 0;
    }
}
