﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bird : BaseBehaviour
{
    [SerializeField]
    private float _speed;

    [SerializeField]
    private float _bufferValue = 2;

    [SerializeField]
    private List<Sprite> _sprites;

    [SerializeField]
    private Image _image;

    private Action[,] _stateActions;

    private bool _rightFacing = true;

    enum eState
    {
        Idle,
        Moving,
        Hit,
    }

    enum eAction
    {
        Update,
        Enter,
    }

    private eState _currentState = eState.Idle;
    
    private void Awake()
    {
        _stateActions = new Action[,]
        {
                            /*Update*/  /*Enter*/
            /*Idle*/    {   Idle        , IdleEnter},
            /*Moving*/  {   Moving      , MovingEnter},
            /*Hit*/     {   Hit         , HitEnter},
        };

        ChangeState(eState.Idle);
    }

    

    private void IdleEnter()
    {
        _image.sprite = _sprites[(int) _currentState];
    }

    private void Idle()
    {
        if(Mathf.Abs(Input.mousePosition.x - transform.position.x) >= _bufferValue)
        {
            ChangeState(eState.Moving);            
        }
    }

    private void ChangeState(eState eState)
    {
        _currentState = eState;
        _stateActions[(int)_currentState, (int)eAction.Enter]();
    }

    private void MovingEnter()
    {
        CheckFacing();
        _image.sprite = _sprites[(int)_currentState];
    }

    private void CheckFacing()
    {
        _rightFacing = Input.mousePosition.x > transform.position.x;
        transform.localScale = !_rightFacing ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);
    }

    private void Moving()
    {
        CheckFacing();

        float moveValue = _speed * Time.deltaTime * (_rightFacing ? 1 : -1);

        if (Mathf.Abs(Input.mousePosition.x - transform.position.x) > Mathf.Abs(moveValue))
        {
            Vector3 pos = transform.position;
            pos.x += moveValue;
            transform.position = pos;
        }
        else
        {
            Vector3 pos = transform.position;
            pos.x = Input.mousePosition.x;
            transform.position = pos;
            ChangeState(eState.Idle);
        }
    }


    private void HitEnter()
    {
        _image.sprite = _sprites[(int)_currentState];
        StartCoroutine(GoToIdle());
    }

    private IEnumerator GoToIdle()
    {
        yield return new WaitForSeconds(0.5f);
        ChangeState(eState.Idle);
    }

    private void Hit() { }


    private void Update()
    {
        _stateActions[(int)_currentState, (int)eAction.Update]();
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Ball")
        {
            ChangeState(eState.Hit);
        }
    }
}
