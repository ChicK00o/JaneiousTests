﻿
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BrickHolder : BaseBehaviour
{
    private GameHandler _gameHandler;

    [SerializeField]
    private GameObject _brickPrefab;

    [SerializeField]
    private List<GameObject> _allBricks = new List<GameObject>();

    [Inject]
    void Construct(GameHandler gameHandler)
    {
        _gameHandler = gameHandler;
    }

    private void PlaceAllBricks()
    {
        DeleteAllBricks();
        float brickWidth = _brickPrefab.GetComponent<RectTransform>().sizeDelta.x;
        float brickHeight = _brickPrefab.GetComponent<RectTransform>().sizeDelta.y;

        float maxWidth = GetComponent<RectTransform>().rect.width;
        float maxHeight = GetComponent<RectTransform>().rect.height;

        int maxColumn = Mathf.FloorToInt(maxWidth / brickWidth);
        int maxRow = Mathf.FloorToInt(maxHeight / brickHeight);

        bool offset = false;

        for(int i = 0; i < maxRow; i++)
        {
            for(int j = 0; j < maxColumn; j++)
            {
                GameObject brickOb = Instantiate(_brickPrefab);
                brickOb.transform.SetParent(transform,false);
                _allBricks.Add(brickOb);
                brickOb.transform.localPosition = new Vector3(
                    ((offset ? -1 : 1) * brickWidth * 0.10f + brickWidth * 0.20f) + (j * brickWidth),
                    -1 * i * brickHeight,
                    0);
            }
            offset = !offset;
        }
    }

    private void DeleteAllBricks()
    {
        foreach(GameObject brick in _allBricks)
        {
            DestroyImmediate(brick);
        }
        _allBricks.Clear();
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem("CONTEXT/BrickHolder/Create bricks")]
    static void CustomMenu()
    {
        Debug.Log("Custom menu called");
        GameObject go = UnityEditor.Selection.activeGameObject;
        BrickHolder holder = go.GetComponent<BrickHolder>();
        holder.PlaceAllBricks();
    }
#endif

    public void RemoveBrick(GameObject o)
    {
        _allBricks.Remove(o);
        if(_allBricks.Count <= 0)
        {
            _gameHandler.GameWin();
        }
    }
}
