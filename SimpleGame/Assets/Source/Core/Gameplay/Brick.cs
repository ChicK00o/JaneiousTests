﻿
using UnityEngine;

public class Brick : BaseBehaviour
{
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Ball")
        {
            transform.parent.GetComponent<BrickHolder>().RemoveBrick(this.gameObject);
            Destroy(this.gameObject);
        }
    }
}
