﻿public class GameSceneManager
{
    // IMP :: Ensure that enums are same as the build setting order index number
    enum eScenes
    {
        MainMenu,
        GameScene,
    }

    public void GoToGameScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene((int)eScenes.GameScene);
    }

    public void GoToMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene((int)eScenes.MainMenu);
    }
}
