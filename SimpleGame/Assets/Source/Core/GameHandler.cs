﻿
using UnityEngine;
using Zenject;

public class GameHandler
{
    private Ball _ball;
    private GameMenu _gameMenu;

    [Inject]
    public GameHandler(Ball ball, GameMenu gameMenu)
    {
        _ball = ball;
        _gameMenu = gameMenu;
    }

    public void GameWin()
    {
        _ball.StopMovement();
        Debug.Log("Game Won");
        _gameMenu.SetupWinning();
    }

    public void LostGame()
    {
        _ball.StopMovement();
        Debug.Log("Game Lost");
        _gameMenu.SetupLosing();
    }
}
