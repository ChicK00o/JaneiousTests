﻿
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GameMenu : BaseBehaviour
{
    private const string _wintext = "You have Won the Game";
    private const string _losetext = "You have Lost the Game";

    [SerializeField]
    private Text _text;

    private GameSceneManager _gameSceneManager;

    [Inject]
    void Construct(GameSceneManager gameSceneManager)
    {
        _gameSceneManager = gameSceneManager;
    }

    public void SetupWinning()
    {
        _text.text = _wintext;
        gameObject.SetActive(true);
    }

    public void SetupLosing()
    {
        _text.text = _losetext;
        gameObject.SetActive(true);
    }

    public void OnQuitPressed()
    {
        _gameSceneManager.GoToMainMenu();
    }
}
