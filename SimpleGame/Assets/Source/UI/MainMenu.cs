﻿using UnityEngine;
using Zenject;

public class MainMenu : MonoBehaviour
{

    private GameSceneManager _gameGameSceneManager;

    [Inject]
    void Construct(GameSceneManager gameSceneManager)
    {
        _gameGameSceneManager = gameSceneManager;
    }

    public void OnStartGame()
    {
        _gameGameSceneManager.GoToGameScene();
    }

    public void OnClose()
    {
        Application.Quit();
    }
}
