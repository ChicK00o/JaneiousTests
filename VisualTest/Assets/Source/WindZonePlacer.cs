﻿using System.Collections.Generic;
using UnityEngine;

public class WindZonePlacer : BaseBehaviour
{
    [SerializeField]
    private GameObject _windZonePrefab;

    [SerializeField]
    List<GameObject> _allWindZone = new List<GameObject>();

    [SerializeField]
    private int _length;

    [SerializeField]
    private float _gap;

    private void PlaceAllWindZones()
    {
        foreach (GameObject o in _allWindZone)
        {
            DestroyImmediate(o);
        }
        _allWindZone.Clear();

        for (int i = 0; i < _length; i++)
        {
            GameObject windZone = Instantiate(_windZonePrefab);
            windZone.transform.SetParent(transform);
            windZone.transform.localPosition = new Vector3(0, 0, i * _gap);
            _allWindZone.Add(windZone);
        }
    }


#if UNITY_EDITOR
    [UnityEditor.MenuItem("CONTEXT/WindZonePlacer/Create and set all the windZone")]
    static void CustomMenu()
    {
        Debug.Log("Custom menu called");
        GameObject go = UnityEditor.Selection.activeGameObject;
        WindZonePlacer holder = go.GetComponent<WindZonePlacer>();
        holder.PlaceAllWindZones();
    }
#endif

}
