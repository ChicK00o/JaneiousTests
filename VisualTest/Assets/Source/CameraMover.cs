﻿
using System;
using UnityEngine;

public class CameraMover : BaseBehaviour
{
    private Action[,] _stateActions;

    enum eState
    {
        None,
        NoTouch,
        Touch,
    }

    enum eAction
    {
        Update,
    }

    private eState _currentState = eState.None;
    private Vector3 _lastMousePosition;

    [SerializeField]
    private float _rotationSpeed;

    [SerializeField]
    private bool _invertY;


    private void Awake()
    {
        _stateActions = new Action[,]
        {
            /*None*/    { () => { } },
            /*NoTouch*/ { NoTouchUpdate },
            /*Touch*/   { TouchUpdate },
        };

        _currentState = eState.NoTouch;
    }

    private void TouchUpdate()
    {
        if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            Vector3 delta = Input.mousePosition - _lastMousePosition;
            _lastMousePosition = Input.mousePosition;
            Vector3 rotation = transform.localEulerAngles;
            rotation.x += delta.y * _rotationSpeed * (_invertY ? -1 : 1);
            rotation.y += delta.x * _rotationSpeed;
            transform.localEulerAngles = rotation;
        }
        else
        {
            _currentState = eState.NoTouch;
        }
    }

    private void NoTouchUpdate()
    {
        if(Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
        {
            _currentState = eState.Touch;
            _lastMousePosition = Input.mousePosition;
        }
    }

    
    private void Update()
    {
        _stateActions[(int)_currentState, (int)eAction.Update]();
    }
}
